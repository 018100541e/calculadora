<?php

    class Calculadora
    {
        //atrib
        public $nro1;
        public $nro2;

        //method
        public function Sumar()
        {
            //return $nro1 + $nro2;
            return $this->nro1 + $this->nro2;
        }
        public function Restar()
        {
            return $this->nro1 - $this->nro2;
        }
        public function Multiplicar()
        {
            return $this->nro1 * $this->nro2;
        }
        public function Dividir()
        {
            if($this->nro2 == 0)
            {
                return "No se puede dividr entre 0";
            }
            else
            {
                return $this->nro1 / $this->nro2;
            }
        }
        
        private function Fact($nro)
        {
            if($nro == 0)
                return 1;
            else
                return $nro * $this->Fact($nro - 1);
        }

        public function Factorial()
        {
            return $this->Fact($this->nro1);
        }
        // Potencia, Seno, Tangente.

    
        private function Pote($base, $exp)
        {
            if($exp == 0)
            {
                return 1;
            }
            else{ 
                return $base * $this->Pote($base , $exp - 1);
            } 
        }
        public function Potencia()
        {
            return $this->Pote($this->nro1,$this->nro2);
        }
        private function Sen($nro) {
            return (sin(deg2rad($nro)));
        }
        public function Seno()
        {
            return $this->Sen($this->nro1);
        }
        private function Tan($nro) {
            return (tan(($nro * pi()) / 180));
        }
        public function Tangente()
        {
            return $this->Tan($this->nro1);
        }

        private function Cos($nro) {
            return (cos(($nro * pi()) / 180));
        }
        public function Coseno()
        {
            return $this->Cos($this->nro1);
        }
        public function Porcentaje()
        {
            return $this->nro1 * $this->nro2/100;
        }
        public function Raiz()
        {
            return  sqrt($this->nro1);
        }
        public function RaizN()
        {
            
            return  sqrt($this->nro1^(1/$this->nro2));
        } 
        public function Inversa()
        {
            
            return  1/($this->nro1);
        }
    }

?>
